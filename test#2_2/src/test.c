/** @file test.c
 *  @brief Count inversions in array.
 */

#include <stdlib.h>
#include <stdint.h>

#include "openedu.h"

typedef int_fast32_t i32;
typedef int_fast64_t i64;

typedef struct {
    i64 *array;
    i64 *buffer;
    i32 size;
} data_t;

i32 saturate(i32 value, i32 boundary)
{
    return value < boundary ? value : boundary;
}

i64 merge_n_count(data_t *data_handle, i32 offset, i32 scrap_size)
{
    if (offset == data_handle->size - 1)
    {
        return 0;
    }
    i64 *arr = data_handle->array;
    i64 *buf = data_handle->buffer;
    i64 counter = 0;
    i32 items;

    i32 i = offset;
    i32 n = offset + scrap_size;

    i32 j = n;
    i32 m = saturate(n + scrap_size, data_handle->size);

    while ((i < n) || (j < m))
    {
        if ((i == n) || ((j < m) && (arr[i] > arr[j])))
        {
            *buf++ = arr[j++];
            counter += n - i;
        }
        else
        {
            *buf++ = arr[i++];
        }
    }

    items = sizeof(*data_handle->buffer) * (scrap_size + m - n);
    memcpy(data_handle->array + offset, data_handle->buffer, items);

    return counter;
}

void count_inversions(data_t *data_handle)
{
    i32 scrap_size = 1;
    i64 counter = 0;
    while ((data_handle->size - 1) / scrap_size)
    {
        i32 offset = 0;
        while (offset + scrap_size < data_handle->size)
        {
            counter += merge_n_count(data_handle, offset, scrap_size);
            offset += 2 * scrap_size;
        }
        scrap_size *= 2;
    }
    println_i64(counter);
}

void get_data(data_t *data_handle)
{
    i32 size = next_i32(-1);
    size_t volume = sizeof(i64) * size;
    i64 *array = (i64*)malloc(volume);
    i64 *buffer = (i64*)malloc(volume);
    
    data_handle->array = array;
    data_handle->buffer = buffer;
    data_handle->size = size;

    while (size--)
    {
        *array++ = next_i64(-1);
    }
}

i32 main(void)
{
    data_t data;

    openedu_io_open();
    get_data(&data);
    count_inversions(&data);
    openedu_io_close();

    return 0;
}


